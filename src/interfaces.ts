export type URL = string;

export interface Ticket {
  id: string,
  name: string,
}

export interface Showing {
  id: string,
  title: string,
  start: Date,
  viewers: Ticket[],
  ///////////////////////////
  boardImage?: string,
  boardDirty: boolean,
}

export interface Theatre {
  id: string,
  name: string,
  showings: Showing[]
}
