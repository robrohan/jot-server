import { Theatre } from './interfaces';

////////////////////////////////////////

const MockTheatre = {
  id: "8ace8041-840b",
  name: "Cinema 1",
  showings: [],
} as Theatre;

export default MockTheatre;
