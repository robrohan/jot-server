import * as WebSocket from 'ws';
import { Showing, Ticket } from './interfaces';
import data from './demo-db';

/////////////////////////////////////
export interface IUICommand {
  type: 'init' | 'disconnect'  // For socket creation / removal
  | 'response'                 // Generic
  | 'boardsync'                // to keep image in sync - push the board image
  | 'boardsend'                // receive the board image form the client
  | 'join'                     // someone joined the theatre
  | 'chat'                     // chat message
  | 'showing'                  // message to ask about showing
  ;
  payload?: any
}
/////////////////////////////////////

export interface FomoSocket extends WebSocket {
  id: string,          // This id is the id of the (socket / ip / browser)
  showingId: string,   // This is the show they are watching / chatting in
}

export interface FomoSocketServer extends WebSocket.Server {
  /* send to everyone on the system everywhere */
  broadcast: (data: IUICommand) => void,
  /* send to a specific connection (for a response for example) */
  sendTo: (socketId: string, IUICommand) => void,
  /* send to a whole theatre of people (channel) */
  sendToTheatre: (showingId: string, data: IUICommand) => void,
  /* get connections to all people in a theatre */
  attendees: (showingId: string) => FomoSocket[],
  pingAll: () => void,
};

export interface SocketRequest {
  ip: string,
  socket: FomoSocket,
  message: WebSocket.Data,
  referrer: string,
};

interface Server {
  log: Function,
  err: Function
  send: Function,
  s4: Function,
  uniqueId: Function,
  onMessage: (wss: FomoSocketServer, sr: SocketRequest) => SocketRequest,
  handleMessage: (wss: FomoSocketServer, sr: SocketRequest, message: IUICommand) => void,
  /////
  sendNext: (wss: FomoSocketServer) => void,
}

const Server = {} as Server;

// Log to sysout
// Object -> Object
Server.log = (data) => {
  console.log([new Date(Date.now()), ':: %s'].join(''), data);
  return data;
};

// Log error with stack trace
// Object -> Object
Server.err = (data) => {
  console.trace([new Date(Date.now()), ':: %s'].join(''), data);
  return data;
};

// Send a socket message 
// SocketMessage -> SocketMessage
Server.send = (sm) => {
  if (sm.socketReq.socket.readyState === WebSocket.OPEN) {
    sm.socketReq.socket.send(sm.message, (err) => {
      if (err) {
        Server.log(err);
      }
    });
  } else {
    Server.log('Tried to send to a closed socket');
    Server.err(sm);
  }

  return sm;
};

// Create a random string (for use in an id)
// void -> string
Server.s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);

// Create a random ID
// void -> string
Server.uniqueId = (): string => [Server.s4(), Server.s4(), '-', Server.s4()].join('');

// Handle messages from a client
// SocketRequest -> SocketRequest
Server.onMessage = (wss: FomoSocketServer, sr: SocketRequest) => {
  // Server.log('Received: ' + sr.message);
  let jMessage = {} as IUICommand;

  try {
    let cleanData = sr.message.toString();
    jMessage = JSON.parse(cleanData);
    Server.handleMessage(wss, sr, jMessage);
  } catch (e) {
    Server.log('Request JSON parse error, this might be ok.');
    Server.err(e);
  }

  return sr;
};

//////
const findShowing = (id?: string): Showing | undefined => {
  if (!id) return undefined;
  for (let x = 0; x < data.showings.length; x++) {
    if (data.showings[x].id === id) {
      return data.showings[x];
    }
  }
  return undefined;
}
//////

// Called when we get a message in from the client
Server.handleMessage = (wss: FomoSocketServer, sr: SocketRequest, message: IUICommand) => {
  switch (message.type) {
    case 'showing': {
      let showing = findShowing(message?.payload?.id)

      if (!showing) {
        const show = {
          id: message?.payload?.id || 'Unknown',
          title: message?.payload?.id || 'Unknown',
          start: new Date(),
          viewers: [],
        } as Showing;
        show.viewers.push({ id: sr.socket.id, name: 'Unknown' } as Ticket);

        data.showings.push(show);
        showing = show;
      } else {
        showing.viewers.push(
          { id: sr.socket.id, name: 'Unknown' } as Ticket
        );
        // Reset the clock to give another 30 minutes
        showing.start = new Date();
        // TODO: someone is joining - not sure this is the
        // best place for this.
        wss.sendToTheatre(showing.id, {
          type: 'join',
          payload: {
            id: sr.socket.id,
            name: 'Unknown'
          }
        } as IUICommand)
      }

      sr.socket.showingId = showing.id;
      wss.sendTo(sr.socket.id, {
        type: 'showing',
        payload: showing,
      } as IUICommand);
    } break;

    case 'chat': {
      // We need to filter on showing at some point, but we are not
      // saving that anywhere. Just broadcast all
      wss.sendToTheatre(
        sr?.socket?.showingId,
        {
          type: 'chat',
          payload: message.payload
        } as IUICommand)
    } break;

    case 'boardsend': {
      const showing = findShowing(sr?.socket?.showingId);
      if (showing) {
        showing.boardImage = message?.payload?.boardImage;
        if (showing.boardImage) showing.boardDirty = true;
      }
    } break;

    case 'disconnect': {
      const showing = findShowing(sr?.socket?.showingId);
      if (showing) {
        showing.viewers = showing.viewers.filter(t => !(t.id === sr.socket.id));
      }
      Server.log("Disconnect:" + ' ' + sr.socket.id + ' ' + sr.socket.showingId);
    } break;

    default: {
      if ((message as any).type === '') {
        // This is a ping message basically.
      }
      Server.log("Unhandled:", sr.socket.id, message);
    }
  }
};

// Like a main game loop. Runs every second
// Everything here sends to *everyone*
Server.sendNext = (wss: FomoSocketServer) => {
  const now = Math.round(Date.now() / 1000); // in seconds

  ////////////////////////////////
  for (let z = 0; z < data.showings.length; z++) {
    const s = data.showings[z];
    if (s == undefined) continue;

    const st = Math.round(s.start.getTime() / 1000); // in seconds

    // TODO: Meeting times end in 30 minutes?
    // from the last entered person. This is to clean
    // up the boardImage memory
    const endTime = st + 30 * 60;

    if (now >= endTime) {
      data.showings[z].boardImage = undefined;
      continue;
    }

    // If we are past the start time
    if (now > st) {
      let runningTime = st;
      if (s.boardDirty) {
        wss.sendToTheatre(
          s.id,
          {
            type: 'boardsync',
            payload: {
              boardImage: s.boardImage,
              time: now - runningTime, // in seconds
            }
          } as IUICommand
        );
        s.boardDirty = false;
      }
    }
  }

  if (now % 5 == 0) {
    wss.pingAll();
  }
}

export default Server;
