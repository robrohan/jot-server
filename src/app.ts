import * as WebSocket from 'ws';
import Server, { SocketRequest, FomoSocketServer, FomoSocket, IUICommand } from './server';
import url from 'url';

const WEBSOCKET_PORT = +process.env.PORT || 8888;

//////////////////////////////////////////////////////////////////////////
const wss = new WebSocket.Server({
  port: WEBSOCKET_PORT,
  verifyClient: (info, cb) => {
    // Grab the URL from the request before it is upgraded to a websocket
    const uri = url.parse(info.req.url);
    const referrer = [info.origin, '/', uri.pathname].join('');
    // Hack
    (info.req as any).referrer = referrer;
    cb(true);
  }
}) as FomoSocketServer;


// TODO: filter on rooms etc
(wss as FomoSocketServer).broadcast = (data: IUICommand) => {
  wss.clients.forEach((client) => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(JSON.stringify(data), (err) => {
        if (err) {
          Server.err(err);
        }
      });
    }
  });
};

(wss as FomoSocketServer).pingAll = () => {
  wss.clients.forEach((client) => {
    client.ping();
  });
}

(wss as FomoSocketServer).sendTo = (socketId: string, data: IUICommand) => {
  // TODO: this wont scale :-/
  wss.clients.forEach((client) => {
    const socket = client as FomoSocket;
    if (socket.readyState === WebSocket.OPEN && socket.id === socketId) {
      socket.send(JSON.stringify(data), (err) => {
        if (err) {
          Server.err(err);
        }
      });
    }
  });
};

(wss as FomoSocketServer).sendToTheatre = (showingId: string, data: IUICommand) => {
  // Server.log("Sending to theatre: " + showingId);
  // TODO: this wont scale :-/
  wss.clients.forEach((client) => {
    const socket = client as FomoSocket;
    // Server.log("to--> " + socket.id + " " + socket.showingId);
    if (socket.readyState === WebSocket.OPEN && socket.showingId === showingId) {
      socket.send(JSON.stringify(data), (err) => {
        if (err) {
          Server.err(err);
        }
      });
    }
  });
};

(wss as FomoSocketServer).attendees = (showingId: string): FomoSocket[] => {
  const clients = [];

  wss.clients.forEach(client => {
    const socket = client as FomoSocket;
    if (socket.readyState === WebSocket.OPEN && socket.showingId === showingId) {
      clients.push(socket);
    }
  });

  return clients;
}

// Handle a connection
wss.on('connection', (websocket: WebSocket, req) => {
  const ws = websocket as FomoSocket;
  ws.id = Server.uniqueId();

  const ip = req.connection.remoteAddress;
  const referrer = (ws.url || req.url);
  Server.log('Connection opened: ' + ip);
  Server.log('\tProtocol: ' + (ws.protocol || 'None'));
  Server.log('\tBinaryType: ' + ws.binaryType);
  Server.log('\tUrl: ' + referrer);
  Server.log('\tId: ' + ws.id);

  ws.on('close', () => {
    Server.log('Connection closed: ' + ip + ' ' + ws.id);
    const socketRequest = {
      ip: ip,
      socket: ws,
      message: JSON.stringify({ type: 'disconnect' }),
      referrer: referrer,
    } as SocketRequest;
    Server.onMessage(wss, socketRequest);
  });

  ws.on('message', (message: WebSocket.Data) => {
    const socketRequest = {
      ip: ip,
      socket: ws,
      message: message,
      referrer: referrer,
    } as SocketRequest;

    Server.onMessage(wss, socketRequest);
  });
});

//////////////////////////////////////////////////////////////////////////

Server.log('Websocket listening on port ' + WEBSOCKET_PORT);
setInterval(() => { Server.sendNext(wss) }, 1000);
