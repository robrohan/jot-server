
install:
# We are using NPM instead of yarn so we don't have to put
# yarn on the docker image.
	npm install

# build: install
# npm run build

publish:
# ./publish.sh

clean:
# rm -rf dist

test:
# npm run test

start: install
	npm run start:dev

build: install
	docker build -t jotcanvas-server .

docker_start:
	docker run -p 8443:8443 jotcanvas-server

remove_mac_files:
	find ./ -name ".DS_Store" -exec rm {} \;

release: clean remove_mac_files test build publish
